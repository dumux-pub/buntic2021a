# Background copied donea test to ensure appl works within pub module
dumux_add_test(NAME test_ff_stokes_donea_copy
               SOURCES main.cc
               CMAKE_GUARD HAVE_UMFPACK
               COMPILE_DEFINITIONS ENABLECACHING=1
               CMD_ARGS params.input)

dune_symlink_to_source_files(FILES "params.input")
