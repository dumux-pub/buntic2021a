# One click install script for dumux
echo " "
echo " "
echo "*********************************************************************************************"
echo "(0/3) Checking all prerequistes. (git gcc g++ cmake pkg-config paraview)"
echo "*********************************************************************************************"

# check some prerequistes
for PRGRM in wget git gcc g++ cmake pkg-config; do
    if ! [ -x "$(command -v $PRGRM)" ]; then
        echo "ERROR: $PRGRM is not installed." >&2
        exit 1
    fi
done

if ! [ -x "$(command -v paraview)" ]; then
    echo "*********************************************************************************************"
    echo "WARNING: paraview seems to be missing. You may not be able to view simulation results!" >&2
    echo "*********************************************************************************************"
fi

currentver="$(gcc -dumpversion)"
requiredver="7"
if [ "$(printf '%s\n' "$requiredver" "$currentver" | sort -V | head -n1)" != "$requiredver" ]; then
    echo "gcc greater than or equal to $requiredver is required for dumux releases >=3.2!" >&2
    exit 1
fi

if [ $? -ne 0 ]; then
    echo "*********************************************************************************************"
    echo "(0/3) An error occured while checking for prerequistes."
    echo "*********************************************************************************************"
    exit $?
else
    echo "*********************************************************************************************"
    echo "(1/3) All prerequistes found."
    echo "*********************************************************************************************"
fi


# make a new folder containing everything
mkdir -p $(pwd)/Dumux_Buntic2021a
cd Dumux_Buntic2021a

echo "*********************************************************************************************"
echo "(1/3) Cloning repositories. This may take a while. Make sure to be connected to the internet."
echo "*********************************************************************************************"

#dune-common
#releases/2.7 # aa689abba532f40db8f5663fa379ea77211c1953 # Tue Nov 10 13:36:21 2020 +0000 # Christian Engwer
if [ ! -d "dune-common" ]; then
    git clone https://gitlab.dune-project.org/core/dune-common.git
    cd dune-common
    git checkout releases/2.7
    git reset --hard aa689abba532f40db8f5663fa379ea77211c1953
    cd ..
else
    echo "Skip cloning dune-common because the folder already exists."
fi

#dune-geometry
#releases/2.7 # 9d56be3e286bc761dd5d453332a8d793eff00cbe # Thu Nov 26 23:26:48 2020 +0000 # Christoph Grüninger
if [ ! -d "dune-geometry" ]; then
    git clone https://gitlab.dune-project.org/core/dune-geometry.git
    cd dune-geometry
    git checkout releases/2.7
    git reset --hard 9d56be3e286bc761dd5d453332a8d793eff00cbe
    cd ..
else
    echo "Skip cloning dune-geometry because the folder already exists."
fi

#dune-grid
#releases/2.7 # b7741c6599528bc42017e25f70eb6dd3b5780277 # Thu Nov 26 23:30:08 2020 +0000 # Christoph Grüninger
if [ ! -d "dune-grid" ]; then
    git clone https://gitlab.dune-project.org/core/dune-grid.git
    cd dune-grid
    git checkout releases/2.7
    git reset --hard b7741c6599528bc42017e25f70eb6dd3b5780277
    cd ..
else
    echo "Skip cloning dune-grid because the folder already exists."
fi

#dune-istl
#releases/2.7 # 761b28aa1deaa786ec55584ace99667545f1b493 # Thu Nov 26 23:29:21 2020 +0000 # Christoph Grüninger
if [ ! -d "dune-istl" ]; then
    git clone https://gitlab.dune-project.org/core/dune-istl.git
    cd dune-istl
    git checkout releases/2.7
    git reset --hard 761b28aa1deaa786ec55584ace99667545f1b493
    cd ..
else
    echo "Skip cloning dune-istl because the folder already exists."
fi

#dune-localfunctions
#releases/2.7 # 68f1bcf32d9068c258707d241624a08b771b6fde # Thu Nov 26 23:45:36 2020 +0000 # Christoph Grüninger
if [ ! -d "dune-localfunctions" ]; then
    git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
    cd dune-localfunctions
    git checkout releases/2.7
    git reset --hard 68f1bcf32d9068c258707d241624a08b771b6fde
    cd ..
else
    echo "Skip cloning dune-localfunctions because the folder already exists."
fi

# dune-subgrid
#releases/2.7 # 45d1ee9f3f711e209695deee97912f4954f7f280 # Thu May 28 13:21:59 2020 +0000 # Oliver Sander
if [ ! -d "dune-subgrid" ]; then
    git clone https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git
    cd dune-subgrid
    git checkout releases/2.7
    git reset --hard 45d1ee9f3f711e209695deee97912f4954f7f280
    cd ..
else
    echo "Skip cloning dune-subgrid because the folder already exists."
fi

# clone pub-module buntic2021a
if [ ! -d "buntic2021a" ]; then
    git clone https://git.iws.uni-stuttgart.de/dumux-pub/buntic2021a.git
else
    echo "Skip cloning pub-module buntic2021a because the folder already exists."
fi

#dumux
#releases/3.3 # 6e26f4c78b731fe0cad3e11d42a26b48919ff2b9 # Wed Mar 10 08:17:35 2021 +0000 # Kilian Weishaupt
if [ ! -d "dumux" ]; then
    git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
    cd dumux
    git checkout releases/3.3
    git reset --hard 6e26f4c78b731fe0cad3e11d42a26b48919ff2b9
    cd ..
else
    echo "Skip cloning dumux because the folder already exists."
fi


if [ $? -ne 0 ]; then
    echo "*********************************************************************************************"
    echo "(1/3) Failed to clone the repositories. Look for repository specific errors."
    echo "*********************************************************************************************"
    exit $?
else
    echo "*********************************************************************************************"
    echo "(2/3) All repositories have been cloned into a containing folder."
    echo "*********************************************************************************************"
fi

echo " "

echo "**************************************************************************************************"
echo "(2/3) Configure and build dune modules and dumux using dunecontrol. This may take several minutes."
echo "**************************************************************************************************"

# run dunecontrol
if [ ! -f "cmake.opts" ]; then
    wget https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/raw/releases/3.3/cmake.opts
else
    echo "A cmake.opts file already exists. The existing file will be used to configure dumux."
fi

./dune-common/bin/dunecontrol --opts=cmake.opts all

if [ $? -ne 0 ]; then
    echo "*********************************************************************************************"
    echo "(2/3) Failed to build the dune libaries."
    echo "*********************************************************************************************"
    exit $?
else
    echo "*****************************************************************************************************"
    echo "(3/3) Succesfully configured and built dune and dumux."
    echo "*****************************************************************************************************"
fi
