Dumux-Pub/Buntic2021a: Source code for Ivan Buntic's Master's Thesis
====================================================================

All branches implement the shear stress transport modell with respect to certain qualities.
The branch **feature/sandGrainRoughness** focusses on flatwall bounded test cases with sandgrain roughness. Also multidomain tests are included.
Next, the branch **feature/verification** emphasizes the verification of the implementation by providing an arbitrarily chosen analytical solution as the initial and boundary conditions.
Finally, the branch **feature/bfs** provides the environment for testing a backwardsfacing step test, which requires non-flat walls.

## Installation from Source
Create a folder in the desired location 

```bash
mkdir DumuxSST
cd DumuxSST
```
and enter it. 

Clone this repository by running
```bash
git clone https://git.iws.uni-stuttgart.de/dumux-pub/buntic2021a.git
```

Run the installscript (within the `DumuxSST/` folder) via
```bash
./buntic2021a/installDumux_Buntic2021a.sh
```

in order to obtain all the necessary dependencies.


Decide on which test cases you wish to try and checkout the respective git branch (one of the following three) by running 

```bash
git checkout feature/sandGrainRoughness
git checkout feature/verification
git checkout feature/bfs
```

in the folder `DumuxSST/buntic2021a/`. After choosing a branch, one still needs to pull the changes compared to the master branch via 

```bash
git pull
```


## Running the tests
In order to run a simulation, navigate to the desired test within the `build-cmake/` folder. For example:

**feature/sandGrainRoughness:** in `/buntic2021a/build-cmake/appl/multidomain/` execute the following commands for the coupled multidomain test by Fetzer

```bash
make test_md_FetzerTest_boundary_darcy2p2c_ransSST1p2c_horizontal
./test_md_FetzerTest_boundary_darcy2p2c_ransSST1p2c_horizontal params_FetzerTest.input
```

or 

in `/buntic2021a/build-cmake/appl/freeflow/rans/sst/lauferpipe/` execute the following commands for Laufer's pipe test

```bash
make test_ff_rans_lauferpipe_sst
./test_ff_rans_lauferpipe_sst params.input
```


**feature/verification:** in `/buntic2021a/build-cmake/appl/freeflow/rans/sst/manufacturedsolutions/` execute the following commands

```bash
make test_ff_rans_sst_analytical
./test_ff_rans_sst_analytical params.input
```

**feature/bfs:** in `/buntic2021a/build-cmake/appl/freeflow/rans/sst/backwardsfacingstep/` execute the following commands for the backwardsfacing step test

```bash
make test_ff_rans_backwardsfacingstep_sst
./test_ff_rans_backwardsfacingstep_sst params_unscaled.input
```

### Installation with Docker

Create a new folder in your favourite location and change into it

```bash
mkdir DumuxSST
cd DumuxSST
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/buntic2021a/-/raw/master/docker_buntic2021a.sh
```

Open the Docker Container
```bash
bash docker_buntic2021a.sh open
```

After the script has run successfully, you may decide on which test cases you wish to try and checkout the respective git branch by running one of the following three:

```bash
cd Dumux_Buntic2021a/buntic2021a
git checkout feature/sandGrainRoughness
git pull
```

```bash
cd Dumux_Buntic2021a/buntic2021a
git checkout feature/verification
git pull
```

```bash
cd Dumux_Buntic2021a/buntic2021a
git checkout feature/bfs
git pull
```

In any branch, the executables can be made by running following commands

```bash
cd build-cmake
make build_tests
```

and you can run them individually. 

**feature/sandGrainRoughness:** One may execute the following commands for the coupled multidomain test by Fetzer

```bash
cd appl/multidomain
./test_md_FetzerTest_boundary_darcy2p2c_ransSST1p2c_horizontal params_FetzerTest.input
```

or 

for Laufer's pipe test

```bash
cd appl/freeflow/rans/sst/lauferpipe
./test_ff_rans_lauferpipe_sst params.input
```

**feature/verification:** 

```bash
cd appl/freeflow/rans/sst/manufacturedsolutions
./test_ff_rans_sst_analytical params.input
```

**feature/bfs:** The following commands can be executed for the backwardsfacing step test

```bash
cd appl/freeflow/rans/sst/backwardsfacingstep
./test_ff_rans_backwardsfacingstep_sst params_unscaled.input
```


## History of development
- k-omega model was taken as basis, SST model was build on top of this implementation
- all the parameters for the BSL- and SST- model are included
- there is a runtime parameter "Problem.TurbulenceModel" which decides which code to use,
  either take the BSL or the SST implementation
- different calculations and different constant parameters of the eddy viscosity for the BSL- and SST-model were included
- So far the SST model was validated via the lauferpipe test and its y⁺-u⁺-diagram
- sandGrainRoughness was implemented with equivalent sand grain roughness k, it can be set within the respective .params-file
- for the backwardsfacing step test visit the bfs branch
- the verification-branch aims to verify the results
- the core of the sandGrainRoughness-branch is the coupled multidomain test by Fetzer implemented with the SST model (on this branch the bfs test does not work yet).

